# prometheus-nodexporter-grafana-pod

Try to setup Prometheus with Podman. 

It works quite okay, until you connect it with Graphana. But hosting Graphana with Podman is a nice
way, still. 

Manual setup of Prometheus is preferred.

[Please refer to the blog article for details](https://capjmk.medium.com/how-to-setup-prometheus-grafana-and-node-exporter-using-podman-systemd-ssl-and-a-reverse-proxy-160e9df196d4)

