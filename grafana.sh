podman run \
	-d \
	--name=grafana \
	--restart=always \
        --pod=prometheus-pod \
 docker.io/grafana/grafana-enterprise