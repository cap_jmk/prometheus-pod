podman pod create \
    --name prometheus-pod \
    -p 127.0.0.1:6000:3000 \
    -p 127.0.0.1:9090:9090 \