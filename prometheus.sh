podman run \
    -d \
    --name=prometheus \
    --restart=always \
    --pod prometheus-pod \
 -v ./prometheus.yml:/etc/prometheus/prometheus.yml \
    docker.io/prom/prometheus